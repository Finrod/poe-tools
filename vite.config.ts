import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'

// https://vitejs.dev/config/
export default defineConfig({
  server: { port: 8090 },

  base: '/poe-tools/',

  resolve: { alias: { '@': '/src' } },

  plugins: [vue(), ViteImageOptimizer()],

  define: {
    'import.meta.env.__APP_VERSION__': JSON.stringify(process.env.npm_package_version)
  }
})
