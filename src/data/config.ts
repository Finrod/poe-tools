export const MIN_BG_VERSION = 4
export const MAX_BG_VERSION = 25

export const links: Link[] = [
  {
    name: 'Challenges',
    link: 'https://www.pathofexile.com/account/view-profile/Finrod927/challenges',
    icon: 'poe.png',
    links: [
      {
        name: 'Challenges',
        link: 'https://www.pathofexile.com/account/view-profile/Finrod927/challenges',
        icon: 'challenge.png'
      },
      { name: 'Rewards', link: 'https://youtu.be/YQfg3WjGq_c?si=sHp6ynE4Iqd9Jger', icon: 'portal.png' }
    ]
  },
  { name: 'Filter Blade', link: 'https://www.filterblade.xyz/', icon: 'filterblade.png' },
  {
    name: 'Levelling Guide',
    link: 'https://www.poe-vault.com/guides/quick-reference-leveling-guide-for-path-of-exile',
    icon: 'vault.png'
  },
  {
    name: 'PoE Lab',
    link: 'https://www.poelab.com/',
    icon: 'trial.png',
    links: [
      { name: 'Uber Lab', link: 'https://www.poelab.com/wfbra', icon: 'trial-uber.png' },
      { name: 'Merc Lab', link: 'https://www.poelab.com/riikv', icon: 'trial-merc.png' },
      { name: 'Cruel Lab', link: 'https://www.poelab.com/r8aws', icon: 'trial-cruel.png' },
      { name: 'Normal Lab', link: 'https://www.poelab.com/gtgax', icon: 'trial-normal.png' }
    ]
  },
  { name: 'PoE Ninja', link: 'https://poe.ninja/', icon: 'ninja.png' },
  { name: 'Wealthy Exile', link: 'https://wealthyexile.com/', icon: 'wealthy.png' },
  { name: 'Wiki', link: 'https://www.poewiki.net/', icon: 'wiki.png' }
]

export const craftLinks: Link[] = [
  { name: 'Blight Helper', link: 'http://blight.raelys.com/', icon: 'blight.png' },
  { name: 'PoE DB', link: 'https://poedb.tw/us/Modifiers', icon: 'poedb.png' },
  { name: 'Regex Editor', link: 'https://poe.re', icon: 'regex.png' },
  { name: 'Trade', link: 'https://www.pathofexile.com/trade', icon: 'trade.png' },
  { name: 'Tattoos', link: 'https://www.poewiki.net/wiki/List_of_tattoos', icon: 'tattoos.png' }
]

export const cheatSheets: CheatSheet[] = [
  { name: 'Ascendancy', src: 'ascendancy.png' },
  { name: 'Delve', src: 'delve.png' },
  { name: 'Essences', src: 'essences.png' },
  { name: 'Incursion', src: 'incursion.png' },
  { name: 'Betrayal', src: 'betrayal.png' },
  { name: 'Metamorph', src: 'metamorph.png' },
  { name: 'Heist', src: 'heist.png' },
  { name: 'Sanctum', src: 'sanctum.png' },
  { name: 'Vendor Recipes', src: 'vendor-recipes.png' }
]

export const tempLeague: CheatSheet[] = [{ name: 'Settlers Shipping', src: 'settlers-shipping.png' }]

export const oldLeague: CheatSheet[] = []

/**
 * Types
 */
export type Link = {
  name: string
  icon: string
  link: string
  links?: Link[]
}

export type CheatSheet = {
  name: string
  src: string
}
