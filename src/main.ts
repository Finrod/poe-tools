import FloatingVue from 'floating-vue'
import 'floating-vue/dist/style.css'
import { createApp } from 'vue'

import App from '@/App.vue'
import '@/style/floating-vue.css'

/** **************** Create app ******************/
createApp(App)
  .use(FloatingVue, {
    distance: 15,
    themes: {
      'poe-tools': {
        $extend: 'menu',
        placement: 'bottom'
      }
    }
  })
  .mount('#app')
